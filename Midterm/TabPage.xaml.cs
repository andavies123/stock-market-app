﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Midterm {
    public partial class TabPage : TabbedPage {
        private string stockAPIAddress = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY";
        private string key = "FRX5UEQ3E7Y8PWBM";

        public string currentStockSymbol;
        public List<StockWithDate> currentStockInfo;

        public TabPage() {
            InitializeComponent();
        }

        public async void GetStocks(string symbol) {
            HttpClient client = new HttpClient();
            Uri uri = new Uri(stockAPIAddress + "&symbol=" + symbol.ToUpper() + "&apikey=" + key);
            StockModel stockData = new StockModel();

            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode) {
                string jsonContent = await response.Content.ReadAsStringAsync();

                if (jsonContent.Contains("Error")) {
                    await DisplayAlert("Invalid symbol", symbol + " is not a valid symbol", "Ok");
                    return;
                }
                stockData = JsonConvert.DeserializeObject<StockModel>(jsonContent);
            }

            stockData.Stocks = new List<StockWithDate>();
            foreach (var item in stockData.TimeSeriesDaily) {
                stockData.Stocks.Add(new StockWithDate {
                    Date = DateTime.Parse(item.Key),
                    StockInfo = item.Value
                });
            }

            currentStockSymbol = symbol.ToUpper();
            currentStockInfo = stockData.Stocks;

            listPage.UpdateList();
            chartPage.UpdateChart();
        }
    }
}
