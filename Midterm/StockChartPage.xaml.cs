﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microcharts;
using Xamarin.Forms;

using Entry = Microcharts.Entry;

namespace Midterm {
    public partial class StockChartPage : ContentPage {
        private enum StockType { Open, Close, Low, High, Volume };

        private int numberOfValues = 10;

        public StockChartPage() {
            InitializeComponent();
            InitGUI();
        }

        public void InitGUI() {
            daysEntry.Text = "30";

            List<StockType> stockTypes = Enum.GetValues(typeof(StockType)).Cast<StockType>().ToList();
            typePicker.ItemsSource = stockTypes;
            typePicker.SelectedIndex = 1;


            symbolEntry.Text = "";
        }

        void OnUnfocus(object sender, Xamarin.Forms.FocusEventArgs e) {
            UpdateChart();
        }

        void OnTextChange(object sender, Xamarin.Forms.TextChangedEventArgs e) {
            Xamarin.Forms.Entry entry = sender as Xamarin.Forms.Entry;
            if(entry == daysEntry) {
                if (int.TryParse(e.NewTextValue, out int value)) return;
                else if (e.NewTextValue == "") entry.Text = "";
                else entry.Text = e.OldTextValue;
            }
            else if(entry == symbolEntry) {
                if (e.NewTextValue.All(Char.IsLetter)) entry.Text = e.NewTextValue.ToUpper();
                else entry.Text = e.OldTextValue;
            }
        }

        void OnUpdateChart(object sender, System.EventArgs e) {
            App.tabPage.GetStocks(symbolEntry.Text);
        }

        public void UpdateChart() {
            if (App.tabPage.currentStockInfo == null) return;
            List<Entry> entries = new List<Entry>();

            int counter = 0;
            int dataDistance = int.Parse(daysEntry.Text) / numberOfValues;
            foreach (StockWithDate stock in App.tabPage.currentStockInfo) {
                String stockInfo = "Open";
                switch(typePicker.SelectedIndex) {
                    case (int)StockType.Open: stockInfo = stock.StockInfo.Open; break;
                    case (int)StockType.Close: stockInfo = stock.StockInfo.Close; break;
                    case (int)StockType.Low: stockInfo = stock.StockInfo.Low; break;
                    case (int)StockType.High: stockInfo = stock.StockInfo.High; break;
                    case (int)StockType.Volume: stockInfo = stock.StockInfo.Volume; break;
                    default: stockInfo = "Close"; break;
                }
                Entry entry;
                if(dataDistance == 0) entry = new Entry(float.Parse(stockInfo)) {ValueLabel = stock.Date.Month + "/" + stock.Date.Day + " : " + stockInfo};
                else if (counter % dataDistance == 0) entry = new Entry(float.Parse(stockInfo)) {ValueLabel = stock.Date.Month + "/" + stock.Date.Day + " : " + stockInfo};
                else entry = new Entry(float.Parse(stockInfo));

                entries.Insert(0, entry);
                counter++;
                if (counter >= int.Parse(daysEntry.Text)) break;
            }

            chartView.Chart = new LineChart() {
                Entries = entries,
                LineMode = LineMode.Straight,
                PointSize = 10,
                LabelTextSize = 50
            };

            symbolEntry.Text = App.tabPage.currentStockSymbol;
        }
    }
}
