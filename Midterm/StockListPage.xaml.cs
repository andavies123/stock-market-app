﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using Xamarin.Forms;

namespace Midterm {
    public partial class StockListPage : ContentPage {

        public StockListPage() {
            InitializeComponent();
        }

        private void OnFetch(object sender, System.EventArgs e) {
            App.tabPage.GetStocks(symbolEntry.Text);
        }

        public void UpdateList() {
            stocksListView.ItemsSource = new ObservableCollection<StockWithDate>(App.tabPage.currentStockInfo);
            symbolEntry.Text = App.tabPage.currentStockSymbol;
            highestLabel.Text = "Highest: " + CalculateHighest(App.tabPage.currentStockInfo);
            lowestLabel.Text = "Lowest: " + CalculateLowest(App.tabPage.currentStockInfo);
        }

        string CalculateHighest(List<StockWithDate> stockList) {
            float highest = 0f;
            foreach(StockWithDate stock in stockList) {
                float currentHigh = float.Parse(stock.StockInfo.High);
                if (currentHigh > highest) highest = currentHigh;
            }
            return highest.ToString();
        }

        string CalculateLowest(List<StockWithDate> stockList) {
            float lowest = 1/0f; // Used to get infinity in c#
            foreach(StockWithDate stock in stockList) {
                float currentLow = float.Parse(stock.StockInfo.Low);
                if (currentLow < lowest) lowest = currentLow;
            }
            return lowest.ToString();
        }

        void OnTextChange(object sender, Xamarin.Forms.TextChangedEventArgs e) {
            Xamarin.Forms.Entry entry = sender as Xamarin.Forms.Entry;
            if (e.NewTextValue.All(Char.IsLetter)) entry.Text = e.NewTextValue.ToUpper();
            else entry.Text = e.OldTextValue;
        }
    }
}